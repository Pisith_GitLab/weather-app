const { default: axios } = require('axios');

require('dotenv').config;

const BASE_URL = process.env.OW_BASE_URL;
const API_KEY = process.env.OW_API_KEY;

const current = async (req, res) => {
  const {city} = req.query;
  try {
    const response = await axios.get(BASE_URL, {
      params: {
        q: city,
        APPID: API_KEY
      }
    });
    if (!response.data) {
      return res.status(500).send({ error: 'Failed to retrieve weather data' });
    }
    return res.send(response.data);
  } catch (error) {
    const errorData = error.response.data;
    return res.status(Number(errorData.cod)).send(errorData.message);
  }
}

module.exports = {
  current
}
