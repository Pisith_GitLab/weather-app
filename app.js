const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const weatherRoute = require('./routes/weatherRoute');

const app = express();
const PORT = process.env.PORT || 3000;

// use bodyParser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.status(200).json({message: 'Weather Application is running'})
})

// use weatherRoute
app.use('/api/v1/weather', weatherRoute);

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
})